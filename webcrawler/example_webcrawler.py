import urllib
from urllib import request,parse
import re


def getHtml(url):
    headers = {
        'User-Agent': r'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      r'Chrome/45.0.2454.85 Safari/537.36 115Browser/6.0.3',
        # 'Referer': r'http://www.lagou.com/zhaopin/Python/?labelWords=label',
        'Connection': 'keep-alive'
    }
    data = {
        'first': 'true',
        'pn': 1,
        'kd': 'Python'
    }
    data = parse.urlencode(data).encode('utf-8')
    req = request.Request(url, headers=headers, data=data)
    html = request.urlopen(req).read()
    html = html.decode('utf-8')
    print(html)

    return html

def getImg(html):
    reg = r'src="data:image/jpeg*"'    #正则表达式，得到图片地址
    imgre = re.compile(reg)     #re.compile() 可以把正则表达式编译成一个正则表达式对象.
    imglist = re.findall(imgre, html)      #re.findall() 方法读取html 中包含 imgre（正则表达式）的    数据

    # urllib.urlretrieve(): 将远程数据下载到本地
    x = 0
    for imgurl in imglist:
        urllib.urlretrieve(imgurl,'D:/Python/workspace/PycharmSpace/utiltools/webcrawler/%s.jpg'%x)
        x+=1

    print(str(x) + ' pictures downloaded.')


html = getHtml("http://image.baidu.com/search/index?tn=baiduimage&ps=1&ct=201326592&lm=-1&cl=2&nc=1&ie=utf-8&word=%E8%B7%91%E6%AD%A5")
getImg(html)