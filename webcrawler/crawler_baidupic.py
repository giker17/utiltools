import os,sys
import urllib
from urllib import request,parse
import json


def getImgsURL(word, pn):
    search = urllib.parse.quote(word)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0',
        'Connection': 'keep-alive'
    }
    data = {
        'first': 'true',
        'pn': 1,
        'kd': 'Python'
    }
    data = parse.urlencode(data).encode('utf-8')
    url = 'http://image.baidu.com/search/avatarjson?tn=resultjsonavatarnew&ie=utf-8&word='\
          + search + '&cg=girl&pn=' + str(pn)\
          + '&rn=60&itg=0&z=0&fr=&width=&height=&lm=-1&ic=0&s=0&st=-1&gsm=1e0000001e'

    imgs = []
    try:
        req = request.Request(url=url, headers=headers, data=data)
        page = request.urlopen(req)
        html = page.read().decode('utf-8')
    except UnicodeDecodeError as e:
        print('-----UnicodeDecodeErrorurl:', url)
    except urllib.error.URLError as e:
        print("-----urlErrorurl:", url)
    else:
        imgs = json.loads(html)['imgs']
    finally:
        page.close()

    return imgs


def downloadImg(imgs, startnum, fileoverwrite=True):
    i = startnum
    pic_path = os.getcwd() + '\\pics\\'
    if not os.path.exists(pic_path):
        os.mkdir(pic_path)
    print('ready for download ' + str(len(imgs)) + ' pictures')
    for img in imgs:
        while not fileoverwrite:
            filename = str(i) + '.jpg'
            if os.path.exists(pic_path + filename):
                print('file ' + filename + ' already exists, generate another filename.')
                i += 1
            else:
                break
        print('dowloading ' + filename)
        try:
            request.urlretrieve(img, pic_path + filename)
        except urllib.error.HTTPError as e:
            print("-----HTTPErrorurl:", img)
            print(e)
        except Exception as err:
            print("unkown exception")
            print(err)
            continue
        finally:
            i += 1

if __name__ == '__main__':
    # 60 pictures every page is default
    objs = getImgsURL('跑步', 2)
    downloadImg(list(map(lambda obj: obj['objURL'], objs)), 0, False)

