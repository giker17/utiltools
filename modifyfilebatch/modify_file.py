import os,sys

homepath = r'D:\SVN\Source\第2課（基幹）\12.生鮮\045-0158_定数表システム\system_merge_20170531\Subsystem_sql\sql\sub'

filepath = os.path.join(homepath, 'type')
rsltpath = os.path.join(homepath, 'rslt')

if not os.path.exists(rsltpath):
    os.mkdir(rsltpath)

for file in [file for file in os.listdir(filepath) if os.path.isfile(os.path.join(filepath, file))]:
    with open(os.path.join(rsltpath, file), 'w+', encoding='utf-16-le') as rf:
        with open(os.path.join(filepath, file), 'r', encoding='utf-16-le') as f:
            f.readline()
            rf.write('USE [SalesPlanDB_Sub]\n')
            rf.writelines(f.readlines())

